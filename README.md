<h1 align="center">万岳外卖系统</h1>

<h5 align="center"></h5>
<div align="center">
 
[![](https://img.shields.io/badge/QQ%E7%BE%A4-995910672-green)](https://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi)

#### 【注意】本外卖系统目前仅有专业版本，开源版本正在开发中！！请联系客服获取演示！


------------------------------------------------------------------------
</div>




### 项目介绍 

万岳科技是同城外卖O2O系统开发商，提供外卖系统源码全套解决方案，包含外卖系统用户端、商户端、骑手端，可支持小程序外卖点餐、后台订单调度、同城跑腿、国际化多语言等服务！
* 提供全套原生外卖系统源码
* 可对系统进行功能定制或二次开发

### 产品介绍
![输入图片说明](images/1.png)
![输入图片说明](images/2.png)
![输入图片说明](images/3.png)

#### 更多开源项目

- 在线教育系统：[查看项目](https://gitee.com/WanYueKeJi/wanyue_education_web)
- 直播商城系统：[查看项目](http://gitee.com/WanYueKeJi/wanyue_zhibo_web)
- 智慧党建系统：[查看项目](http://gitee.com/WanYueKeJi/wanyue_dangjian)
- 知识付费系统：[查看项目](http://gitee.com/WanYueKeJi/Wanyue-knowledge-payment-UNI-APP)


### 专业版咨询 (Add customer service manager to wechat or QQ to get product demo)

<div style='height: 130px'>
    <img class="kefu_weixin" style="float:left;" src="https://gitee.com/WanYueKeJi/wanyue_education_uniapp/raw/newone/pages/%E5%BC%A0%E7%9A%93%E5%BC%80%E6%BA%90.png" width="602" height="123"/>
    <div style="float:left;">
        <p>QQ：2770722087</p>
        <p>QQ群：995910672</p>
        <p>QQ群：681418688</p>

#### 技术交流
 ![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_education_web/raw/master/%E4%B8%87%E5%B2%B3%E7%A7%91%E6%8A%80%E5%BC%80%E6%BA%90%E8%AE%A8%E8%AE%BA15%E7%BE%A4%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
>
